package ru.whitewhale.cache.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import ru.whitewhale.cache.client.SubscriberHttpClient;
import ru.whitewhale.cache.domain.SubInfo;
import ru.whitewhale.cache.repository.SubInfoRepository;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SubscribeServiceImplTest {
    private final List<SubInfo> subInfoList = singletonList(new SubInfo(1L, 2d));

    @Mock
    private SubscriberHttpClient httpClient;
    @Mock
    private SubInfoRepository repository;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @InjectMocks
    private SubscribeServiceImpl subscribeService;

    @Test
    public void shouldSaveSubscribersInfo() {
        given(httpClient.getSubscribersInfo()).willReturn(subInfoList);

        subscribeService.saveSubscribersInfo();

        verify(repository, only()).saveAll(subInfoList);
    }

    @Test
    public void shouldPublishSubscribersInfo() {
        given(repository.findAllByBalanceGreaterThan(anyDouble())).willReturn(subInfoList);

        subscribeService.publishSubscribersInfo();

        verify(rabbitTemplate, only()).convertAndSend(subInfoList);
    }

}