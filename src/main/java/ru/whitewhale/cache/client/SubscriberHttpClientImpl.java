package ru.whitewhale.cache.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.whitewhale.cache.domain.SubInfo;
import ru.whitewhale.cache.domain.Subscriber;
import ru.whitewhale.cache.repository.SubInfoRepository;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@Service
public class SubscriberHttpClientImpl implements SubscriberHttpClient{
    private static final String URI = "http://localhost:8080/subscribers/xml";
    private final Client client;

    public SubscriberHttpClientImpl() {
        this.client = ClientBuilder.newClient();
    }

    public List<SubInfo> getSubscribersInfo() {
        List<Subscriber> subscribers = client.target(URI)
                .request(APPLICATION_XML)
                .get(new GenericType<List<Subscriber>>() {
                });

      return subscribers.stream()
                .map(sub -> new SubInfo(sub.getId(), sub.getAmount()))
                .collect(Collectors.toList());

    }
}
