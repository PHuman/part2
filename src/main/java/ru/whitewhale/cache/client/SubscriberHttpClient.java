package ru.whitewhale.cache.client;

import ru.whitewhale.cache.domain.SubInfo;

import java.util.List;

public interface SubscriberHttpClient {
    List<SubInfo> getSubscribersInfo();
}
