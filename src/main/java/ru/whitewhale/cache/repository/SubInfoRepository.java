package ru.whitewhale.cache.repository;

import ru.whitewhale.cache.domain.SubInfo;
import org.springframework.data.repository.CrudRepository;

public interface SubInfoRepository extends CrudRepository<SubInfo, Long> {

    Iterable<SubInfo> findAllByBalanceGreaterThan(double balance);
}
