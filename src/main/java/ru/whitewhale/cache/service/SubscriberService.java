package ru.whitewhale.cache.service;

public interface SubscriberService {

    void saveSubscribersInfo();

    void publishSubscribersInfo();
}
