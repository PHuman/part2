package ru.whitewhale.cache.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.whitewhale.cache.client.SubscriberHttpClient;
import ru.whitewhale.cache.domain.SubInfo;
import ru.whitewhale.cache.repository.SubInfoRepository;

import java.util.List;

@Service
public class SubscribeServiceImpl implements SubscriberService {
    private final SubscriberHttpClient httpClient;
    private final SubInfoRepository repository;
    private final RabbitTemplate rabbitTemplate;

    @Value("${balance}")
    private double balance;

    @Autowired
    public SubscribeServiceImpl(SubscriberHttpClient httpClient, SubInfoRepository repository, RabbitTemplate rabbitTemplate) {
        this.httpClient = httpClient;
        this.repository = repository;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelayString = "${retrieve.timeout}")
    @Override
    public void saveSubscribersInfo() {
        List<SubInfo> subscribersInfo = httpClient.getSubscribersInfo();
        repository.saveAll(subscribersInfo);
//        System.out.println("save: " + subscribersInfo);
    }

    @Scheduled(fixedDelayString = "${publish.timeout}")
    @Override
    public void publishSubscribersInfo() {
        Iterable<SubInfo> subscribersInfo = repository.findAllByBalanceGreaterThan(balance);
        rabbitTemplate.convertAndSend(subscribersInfo);
//        System.out.println("publish: " + subscribersInfo);
//        Message message = rabbitTemplate.receive("queue");
//        System.out.println(message);
    }
}
