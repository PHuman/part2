package ru.whitewhale.cache.domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
class TariffPlan {
    private Long id;
    private String name;
    private String description;
    private double price;
}
