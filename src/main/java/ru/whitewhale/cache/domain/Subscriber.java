package ru.whitewhale.cache.domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class Subscriber {
    private Long id;
    private String name;
    private String surname;
    private Double amount;
    private TariffPlan tariffPlan;
}
