package ru.whitewhale.cache.domain;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.couchbase.core.mapping.Document;

@Data
@AllArgsConstructor
@Document
public class SubInfo {
    @Id
    private Long id;
    @Field
    private Double balance;
}
